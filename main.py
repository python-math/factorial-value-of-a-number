# Factorial
def fac_of_n(number):
    if number == 1 :
        return number
    else:
        return number * fac_of_n(number-1)

n = int(input('Please enter a number : '))
print(fac_of_n(n))